// this file is here for an example of routing
// routing is automatically done for files in Pages
// routing docs: https://nextjs.org/docs/routing/introduction
import Head from 'next/head'
import dynamic from 'next/dynamic'

const DynamicMap = dynamic(() => import('../components/Map'))

export default function Home() {
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossorigin=""
        />
        <script
          src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
          integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
          crossorigin=""
        ></script>
      </Head>
      <div id="map-container">
        <DynamicMap />
      </div>
    </>
  )
}
