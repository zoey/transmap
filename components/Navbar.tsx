import { isEmpty } from 'lodash'
import { useContext } from 'react'
import { Menu } from 'semantic-ui-react'

import styles from '../styles/Navbar.module.scss'
import { SessionContext } from '../lib/sessionContext'

import LogoutButton from '../components/LogoutButton'
import LoginButton from './LoginButton'
import { MenuItemLink } from './links'

const Navbar = () => {
  const session = useContext(SessionContext)

  return (
    <Menu className={styles.homeMenu}>
      <MenuItemLink href="/" header>
        Home
      </MenuItemLink>
      {!isEmpty(session) && (
        <MenuItemLink href="/profile">Profile</MenuItemLink>
      )}
      <Menu.Item position="right">
        {isEmpty(session) ? <LoginButton /> : <LogoutButton />}
      </Menu.Item>
    </Menu>
  )
}

export default Navbar
