import { Button } from 'semantic-ui-react'
import { supabase } from '../utils/supabaseClient'

const Account = () => (
  <Button content="Log out" onClick={() => supabase.auth.signOut()} secondary />
)

export default Account
