import { useEffect, useState } from 'react'
import { Icon } from 'semantic-ui-react'
import { supabase } from '../utils/supabaseClient'
// import Image from 'next/image'

type AvatarProps = {
  url: string
  size: any
  onUpload: (value: any) => void
}

export default function Avatar({ url, size, onUpload }: AvatarProps) {
  const [avatarUrl, setAvatarUrl] = useState<string>('')
  const [uploading, setUploading] = useState<boolean>(false)

  useEffect(() => {
    if (url) downloadImage(url)
  }, [url])

  async function downloadImage(path: any) {
    try {
      const { data, error } = await supabase.storage
        .from('avatars')
        .download(path)
      if (error) {
        throw error
      }
      const url = URL.createObjectURL(data)
      setAvatarUrl(url)
    } catch (error) {
      console.error('Error downloading image: ', error.message)
    }
  }

  async function uploadAvatar(event: any) {
    try {
      setUploading(true)

      if (!event.target.files || event.target.files.length === 0) {
        throw new Error('You must select an image to upload.')
      }

      const file = event.target.files[0]
      const fileExt = file.name.split('.').pop()
      const fileName = `${Math.random()}.${fileExt}`
      const filePath = `${fileName}`

      let { error: uploadError } = await supabase.storage
        .from('avatars')
        .upload(filePath, file)

      if (uploadError) {
        console.error('error uploading:', uploadError)
        throw uploadError
      }

      onUpload(filePath)
    } catch (error) {
      alert(error.message)
    } finally {
      setUploading(false)
    }
  }

  // TODO: get the Image component to work with localhost
  // we should be using the `Image` component from next, but it throws an error when using localhost
  return (
    <div>
      {!!avatarUrl ? (
        <>
          {/*<Image
            src={avatarUrl}
            alt="Avatar"
            className="avatar image"
            width={size}
            height={size}
          />*/}
          <img
            src={avatarUrl}
            alt="Avatar"
            className="avatar image"
            style={{ height: size, width: size }}
          />
        </>
      ) : (
        <div
          className="avatar no-image"
          style={{ height: size, width: size }}
        />
      )}

      <div style={{ width: size }}>
        <label htmlFor="single">
          <Icon
            name={uploading ? 'spinner' : 'upload'}
            loading={uploading}
            link
          />
        </label>
        <input
          style={{
            visibility: 'hidden',
            position: 'absolute',
          }}
          type="file"
          id="single"
          accept="image/*"
          onChange={uploadAvatar}
          disabled={uploading}
        />
      </div>
    </div>
  )
}
