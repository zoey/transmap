# Transmap.org

A web app for finding transgender healthcare and other resources. This is a work in progress.

## Roadmap

### MVP

- [x] Add map using [OSM](https://react-leaflet.js.org/) or Google Maps
- [ ] Locations
  - [ ] Add location - users can add locations, integrate search using Google places or something similar
  - [ ] Map markers (each location will show up on the map and have a popup with details)
  - [ ] Details page
- [ ] Discord authentication
- [ ] User profile with custom display name.
- [ ] If possible, import all locations from the [existing Google map](https://www.google.com/maps/d/viewer?mid=1DxyOTw8dI8n96BHFF2JVUMK7bXsRKtzA&ll=35.49204780825351%2C-83.10673301287963&z=8)
- [ ] Deploy - with certificate for https

### Extra

- [ ] Location features
  - [ ] Tags (informed consent, community center, etc) and the ability to filter by tags on the map view.
  - [ ] Comments/reviews
  - [ ] Status (users can report status of location)
  - [ ] "Report" flag (so users can report false data)
- [ ] Twitter authentication
- [ ] Search for specific location on the map
- [ ] List view (as an alternative to map view)
- [ ] Custom avatar for users.
- [ ] Get current location from user's browser.
- [ ] SEO optimization.
- [ ] Resources page - with links to helpful transgender resources.
- [ ] About page.
